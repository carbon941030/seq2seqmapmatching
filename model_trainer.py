# -*- coding: utf-8 -*-

import os
import threading

import tensorflow as tf

from seq2seq_model import Seq2SeqModel
from model_config import ModelConfig
from reader import Reader
from utils import get_progress_bar


class ModelTrainer(object):
    def __init__(self, config: ModelConfig):
        self.config = config

        self.train_reader = Reader(config, config.train_file_name, config.train_samples)
        self.valid_reader = Reader(config, config.valid_file_name, config.valid_samples)

        self.global_step = tf.Variable(0, name='global_step', trainable=False)
        self.increase_global_step = tf.assign_add(self.global_step, tf.constant(1))

        self.model = Seq2SeqModel(config)
        self.model.construct(self.train_reader.queue, self.valid_reader.queue, self.global_step)

        checkpoint_duration = config.checkpoint_duration
        if checkpoint_duration < 0:
            checkpoint_duration = 0
        self.sv = tf.train.Supervisor(
            logdir=config.log_dir,
            global_step=self.global_step,
            summary_op=None,
            saver = self.model.saver,
            save_model_secs=checkpoint_duration,
            init_fn=self.init_fn,
            local_init_op=self.model.optimizer_init_op)

    @staticmethod
    def should_stop_by_outerfile(filename) -> bool:
        should_stop = False
        with open(filename, "r") as f:
            command = f.readline()
            if command == "STOP":
                should_stop = True
        return should_stop

    def keep_loading(self, reader, sess: tf.Session):
        config = self.config
        while not ModelTrainer.should_stop_by_outerfile(config.stop_control_filename):
            reader.load_and_enqueue(sess)

    def init_fn(self, sess):
        self.model.load_embeddings(sess)

    def train(self):
        config = self.config

        print('Start training')

        epoch_count = 0
        best_valid_loss = 1e9
        with self.sv.managed_session() as sess:
            train_load_thread = threading.Thread(
                target=self.keep_loading,
                args=(self.train_reader, sess))
            train_load_thread.start()
            valid_load_thread = threading.Thread(
                target=self.keep_loading,
                args=(self.valid_reader, sess))
            valid_load_thread.start()

            while not ModelTrainer.should_stop_by_outerfile(config.stop_control_filename):
                epoch_count += 1
                print('Start epoch %d:' % epoch_count)

                train_loss = 0.0
                total_correct = 0.0
                total_weights = 0.0
                dot_count = max(config.train_batches // 1000, 1)
                with get_progress_bar(config.train_batches, ['loss', 'acc']) as bar:
                    for train_step in range(config.train_batches):
                        feed_dict = {
                            self.model.output_keep_prob: config.output_keep_prob,
                            self.model.select_queue: 0
                        }
                        fetch_list = [
                            self.model.train_op,
                            self.model.loss,
                            self.model.acc,
                            self.model.total_weights,
                        ]
                        if (train_step + 1) % (dot_count * 10) == 0:
                            _, step_loss, step_acc, step_weights, summary = sess.run(
                                fetch_list + [self.model.train_summary],
                                feed_dict=feed_dict)
                            self.sv.summary_computed(sess, summary)
                        else:
                            _, step_loss, step_acc, step_weights = sess.run(
                                fetch_list,
                                feed_dict=feed_dict)
                        train_loss += step_loss
                        total_weights += step_weights
                        total_correct += step_acc * step_weights
                        if (train_step + 1) % (dot_count * 1) == 0:
                            avg_loss = train_loss / (train_step + 1)
                            avg_acc = total_correct / total_weights
                            bar.update(train_step + 1, loss=avg_loss, acc=avg_acc)
                train_loss /= config.train_batches
                train_acc = total_correct / total_weights
                print('Epoch %d, training phase ended, training loss: %f, acc: %f'
                      % (epoch_count, train_loss, train_acc))

                valid_loss = 0.0
                total_correct = 0.0
                total_weights = 0.0
                dot_count = max(config.valid_batches // 1000, 1)
                with get_progress_bar(config.valid_batches, ['loss', 'acc']) as bar:
                    for valid_step in range(config.valid_batches):
                        feed_dict = {
                            self.model.output_keep_prob: 1.0,
                            self.model.select_queue: 1
                        }
                        fetch_list = [
                            self.model.loss,
                            self.model.acc,
                            self.model.total_weights,
                            self.increase_global_step
                        ]
                        if (valid_step + 1) % (dot_count * 10) == 0:
                            step_loss, step_acc, step_weights, _, summary = sess.run(
                                fetch_list + [self.model.valid_summary],
                                feed_dict=feed_dict)
                            self.sv.summary_computed(sess, summary)
                        else:
                            step_loss, step_acc, step_weights, _ = sess.run(
                                fetch_list,
                                feed_dict=feed_dict)
                        valid_loss += step_loss
                        total_weights += step_weights
                        total_correct += step_acc * step_weights
                        if (valid_step + 1) % (dot_count * 1) == 0:
                            avg_loss = valid_loss / (valid_step + 1)
                            avg_acc = total_correct / total_weights
                            bar.update(valid_step + 1, loss=avg_loss, acc=avg_acc)
                valid_loss /= config.valid_batches
                valid_acc = total_correct / total_weights
                print('Epoch %d, validation phase ended, validation loss: %f, acc: %f\n'
                      % (epoch_count, valid_loss, valid_acc))

                if config.checkpoint_duration == 0:
                    if valid_loss < best_valid_loss:
                        best_valid_loss = valid_loss
                        self.model.saver.save(
                            sess,
                            os.path.join(config.log_dir, 'model.ckpt'),
                            global_step=self.global_step)