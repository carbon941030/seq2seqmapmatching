#!/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf

from utils import uniform_choice

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('pts_path', '',
                       'Path of pts file.')
tf.flags.DEFINE_string('edge_path', '',
                       'Path of edge file.')
tf.flags.DEFINE_string('target_file_path', '',
                       'Path of the target file.')

tf.flags.DEFINE_float('keep_ratio', 1.0,
                      'Ratio to keep pts')

def get_traj_pts(pts_file):
    lats = []
    lons = []
    line = pts_file.readline()
    while line[0] != '-':
        data = line.split()
        lats.append(float(data[1]))
        lons.append(float(data[2]))
        line = pts_file.readline()
    return lats, lons


def main(_):
    ratio = FLAGS.keep_ratio
    pts_file = open(FLAGS.pts_path, 'r')
    target_file = open(FLAGS.target_file_path, 'w')
    with open(FLAGS.edge_path) as edge_file:
        for edge_line in edge_file:
            lats, lons = get_traj_pts(pts_file)
            pts = np.array([lats, lons]).transpose()
            pts = np.array([pts[0]] + uniform_choice(pts[1: -1], ratio) + [pts[-1]]).transpose()
            lats, lons = pts
            line = ';'.join(
                [
                    edge_line.strip('\n'),
                    ''.join([str(lat) + ',' for lat in lats]),
                    ''.join([str(lon) + ',' for lon in lons]),
                ])
            print(line, file=target_file)
    pts_file.close()
    target_file.close()

if __name__ == '__main__':
    tf.app.run()
