# -*- coding: utf-8 -*-

import numpy as np
import progressbar


def line2int_list(line: str):
    line = line.split(',')
    line = map(lambda x: x.strip(), line)
    line = filter(lambda x: x != '', line)
    line = map(lambda x: int(x), line)
    return list(line)


def line2float_list(line: str):
    line = line.split(',')
    line = map(lambda x: x.strip(), line)
    line = filter(lambda x: x != '', line)
    line = map(lambda x: float(x), line)
    return list(line)


def get_progress_bar(total, messages):
    widgets = ['|', progressbar.Percentage(), '|']
    widgets += ['(', progressbar.Counter() , ' of %d)'%(total)]
    widgets += [progressbar.Bar()]
    widgets += [progressbar.Timer(), '|']
    widgets += [progressbar.ETA(), '|']
    for message in messages:
        widgets += [progressbar.DynamicMessage(message), '|']
    return progressbar.ProgressBar(max_value=total, widgets=widgets)

def random_choice(lst, ratio):
    res = []
    for item in lst:
        if np.random.random() < ratio:
            res.append(item)
    return res

def uniform_choice(lst, ratio):
    res = []
    sum = 0.0
    for item in lst:
        sum += ratio
        if sum >= 1.0:
            res.append(item)
            sum -= 1.0
    return res
