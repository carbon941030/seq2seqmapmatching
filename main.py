#!/bin/python3
# -*- coding: utf-8 -*-

import configparser
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='1'

import tensorflow as tf

from model_config import ModelConfig
from model_trainer import ModelTrainer

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('config_path', '',
                       'Path of the config file.')

def main(_):
    config_file = configparser.ConfigParser()
    config_file.read(FLAGS.config_path)
    model_base_config = config_file['model_base']
    training_config = config_file['training']
    file_path_config = config_file['file_path']
    reader_config = config_file['reader']

    seq_len = model_base_config.getint('seq_len')
    if seq_len < 0:
        seq_len = None
    assert reader_config.getint('train_samples') % model_base_config.getint('batch_size') == 0
    assert reader_config.getint('valid_samples') % model_base_config.getint('batch_size') == 0
    config = ModelConfig(
        cell_type=model_base_config['cell_type'],
        attention_type=model_base_config['attention_type'],
        hidden_dim=model_base_config.getint('hidden_dim'),
        encode_dim=model_base_config.getint('encode_dim'),
        attention_depth=model_base_config.getint('attention_depth'),
        output_keep_prob=training_config.getfloat('output_keep_prob'),
        batch_size=model_base_config.getint('batch_size'),
        float_type={
            'float16': tf.float16,
            'float32': tf.float32,
            'float64': tf.float64
        }[model_base_config['float_type']],
        seq_len=seq_len,
        vocabulary_size=model_base_config.getint('vocabulary_size'),
        TRAJ_START=model_base_config.getint('vocabulary_size') - 1,
        TRAJ_END=model_base_config.getint('vocabulary_size') - 2,
        embedding_size=model_base_config.getint('embedding_size'),
        embedding_trainable=model_base_config.getboolean('embedding_trainable'),
        queue_capacity=reader_config.getint('queue_capacity'),
        rs_queue_capacity=reader_config.getint('rs_queue_capacity'),
        pts_keep_rate=model_base_config.getfloat('pts_keep_rate'),
        lat_bias=model_base_config.getfloat('lat_bias'),
        lat_scale=model_base_config.getfloat('lat_scale'),
        lon_bias=model_base_config.getfloat('lon_bias'),
        lon_scale=model_base_config.getfloat('lon_scale'),
        train_samples=reader_config.getint('train_samples'),
        train_batches=reader_config.getint('train_samples') // model_base_config.getint('batch_size'),
        valid_samples=reader_config.getint('valid_samples'),
        valid_batches=reader_config.getint('valid_samples') // model_base_config.getint('batch_size'),
        lr=training_config.getfloat('lr'),
        max_gradient_norm=training_config.getfloat('max_gradient_norm'),
        checkpoint_duration=training_config.getint('checkpoint_duration'),
        embeddings_loading_path=file_path_config.get('embeddings_loading_path', fallback=''),
        train_file_name=file_path_config['train_file_name'],
        valid_file_name=file_path_config['valid_file_name'],
        stop_control_filename=file_path_config['stop_control_filename'],
        log_dir=file_path_config['log_dir'])

    trainer = ModelTrainer(config)
    trainer.train()

if __name__ == '__main__':
    tf.app.run()
