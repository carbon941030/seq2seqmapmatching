#!/bin/python3
# -*- coding: utf-8 -*-

import tensorflow as tf

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('source_path', '',
                       'Path of the source file.')
tf.flags.DEFINE_string('target_path', '',
                       'Path of the target file.')


def main(_):
    all_lines = [[] for _ in range(65536)]
    with open(FLAGS.source_path, 'r') as source_file:
        for line in source_file:
            traj, lat, lon = line.split(';')
            pts_len = len(lat)
            all_lines[pts_len].append(line)
    target_file = open(FLAGS.target_path, 'w')
    all_lines.reverse()
    for lines in all_lines:
        for line in lines:
            print(line, end='', file=target_file)
    target_file.close()

if __name__ == '__main__':
    tf.app.run()
