# -*- coding: utf-8 -*-

import math
import numpy as np
import tensorflow as tf

from model_config import ModelConfig
from utils import line2int_list
from utils import line2float_list
from utils import random_choice


class Reader(object):
    def __init__(self, config: ModelConfig, file_name, samples):
        self.config = config
        self.file_name = file_name
        self.samples = samples

        self.rs_queue = tf.RandomShuffleQueue(
            capacity=config.rs_queue_capacity,
            min_after_dequeue=config.rs_queue_capacity - config.queue_capacity,
            dtypes=[tf.int64, tf.int64, tf.int64, tf.int64, config.float_type, config.float_type],
            names=['lens', 'inputs', 'labels', 'pts_lens', 'pts', 'masks'])
        self.queue = tf.PaddingFIFOQueue(
            capacity=config.queue_capacity,
            dtypes=[tf.int64, tf.int64, tf.int64, tf.int64, config.float_type, config.float_type],
            shapes=[[], [None], [None], [], [None, 3], [None]],
            names=['lens', 'inputs', 'labels', 'pts_lens', 'pts', 'masks'],
            name='queue')
        self.enqueue_len = tf.placeholder(dtype=tf.int64, name='enqueue_len')
        self.enqueue_input = tf.placeholder(dtype=tf.int64, name='enqueue_input')
        self.enqueue_label = tf.placeholder(dtype=tf.int64, name='enqueue_label')
        self.enqueue_pts_len = tf.placeholder(dtype=tf.int64, name='enqueue_pts_len')
        self.enqueue_pts = tf.placeholder(dtype=config.float_type, name='enqueue_pts')
        self.enqueue_mask = tf.placeholder(dtype=config.float_type, name='enqueue_mask')
        self.rs_enqueue_op = self.rs_queue.enqueue(
            {
                'lens': self.enqueue_len,
                'inputs': self.enqueue_input,
                'labels': self.enqueue_label,
                'pts_lens': self.enqueue_pts_len,
                'pts': self.enqueue_pts,
                'masks': self.enqueue_mask
            })
        self.enqueue_op = self.queue.enqueue(self.rs_queue.dequeue(), name='enqueue_op')
        self.qr = tf.train.QueueRunner(self.queue, [self.enqueue_op])
        tf.train.queue_runner.add_queue_runner(self.qr)

    def load_and_enqueue(self, sess: tf.Session):
        config = self.config

        with open(self.file_name, 'r') as f:
            traj_count = 0
            for line in f:
                traj, lat, lon = line.split(';')
                traj = line2int_list(traj)
                lat = line2float_list(lat)
                lon = line2float_list(lon)
                if config.seq_len is not None:
                    if len(traj) > config.seq_len - 1 or len(traj) < 3:
                        continue
                traj_count += 1
                length = len(traj) + 1
                inputs = np.array([config.TRAJ_START] + traj)
                labels = np.array(traj + [config.TRAJ_END])
                pos = np.linspace(-math.sqrt(3), math.sqrt(3), len(lat))
                pts = np.array([lat, lon, pos]).transpose()
                pts = pts - [config.lat_bias, config.lon_bias, 0.0]
                pts = pts * [config.lat_scale, config.lon_scale, 1.0]
                pts = np.array([pts[0]] + random_choice(pts[1: -1], config.pts_keep_rate) + [pts[-1]])
                pts_len = len(lat)
                masks = np.ones([length], dtype=np.float32)
                sess.run(
                    self.rs_enqueue_op,
                    feed_dict={
                        self.enqueue_len: length,
                        self.enqueue_input: inputs,
                        self.enqueue_label: labels,
                        self.enqueue_pts: pts,
                        self.enqueue_pts_len: pts_len,
                        self.enqueue_mask: masks
                    })
                if traj_count >= self.samples:
                    break

