# -*- coding: utf-8 -*-
import math

import tensorflow as tf

from model_config import ModelConfig


class Seq2SeqModel(object):
    def __init__(self, config: ModelConfig):
        self.config = config

    def construct(
            self,
            train_queue: tf.PaddingFIFOQueue,
            valid_queue: tf.PaddingFIFOQueue,
            global_step):
        config = self.config
        self.define_params()

        self.select_queue = tf.placeholder(dtype=tf.int64)
        queue = tf.QueueBase.from_list(self.select_queue, [train_queue, valid_queue])
        dequeue_dict = queue.dequeue_many(config.batch_size)

        self.loss, \
        self.acc, \
        self.total_weights, \
        self.train_summary, \
        self.valid_summary, \
        self.train_op, \
        self.saver, \
        self.optimizer_init_op = self.build_model(
            dequeue_dict,
            config.batch_size,
            global_step=global_step)

    def get_wb(self, dimin, dimout):
        w = tf.Variable(
            tf.truncated_normal(
                [dimin, dimout],
                stddev=1.0 / math.sqrt(dimin)))
        b = tf.Variable(
            tf.zeros([dimout]))
        return w, b

    def define_params(self):
        config = self.config

        self.output_keep_prob = tf.placeholder(dtype=config.float_type, shape=[], name='output_keep_prob')
        self.define_inputs()
        self.define_embeddings()
        self.define_cell()
        self.define_middleware()
        self.define_output_params()

    def define_inputs(self):
        config = self.config
        self.input_w, self.input_b = self.get_wb(3, config.hidden_dim)

    def define_embeddings(self):
        config = self.config

        self.embeddings = tf.Variable(
            tf.random_uniform([config.vocabulary_size, config.embedding_size], -1, 1),
            name='embeddings')
        if config.embeddings_loading_path != '':
            self.embeddings_saver = tf.train.Saver({'embeddings': self.embeddings})
        if not config.embedding_trainable:
            self.embeddings = tf.stop_gradient(self.embeddings)

    def define_cell(self):
        config = self.config

        cell = {
            'basic': lambda: tf.contrib.rnn.BasicRNNCell(config.hidden_dim),
            'lstm': lambda: tf.contrib.rnn.BasicLSTMCell(config.hidden_dim),
            'gru': lambda: tf.contrib.rnn.GRUCell(config.hidden_dim),
        }[config.cell_type]
        if config.output_keep_prob < 1:
            cell = lambda c=cell: tf.contrib.rnn.DropoutWrapper(c(), output_keep_prob=self.output_keep_prob)

        self.encoder_fw_cells = [cell()]
        self.encoder_bw_cells = [cell()]

        self.decoder_attn_cell = cell()
        self.decoder_top_cell = cell()

    def define_middleware(self):
        config = self.config
        self.m_per_w, self.m_per_b = self.get_wb(config.hidden_dim * 4, config.encode_dim)

    def define_output_params(self):
        config = self.config

        self.output_w = tf.Variable(
            tf.truncated_normal(
                [config.vocabulary_size, config.hidden_dim],
                stddev=1.0 / math.sqrt(config.hidden_dim)),
            'output_w')
        self.output_b = tf.Variable(
            tf.zeros([config.vocabulary_size]),
            'output_b')

    def load_embeddings(self, sess):
        config = self.config
        if config.embeddings_loading_path != '':
            self.embeddings_saver.restore(sess, config.embeddings_loading_path)

    def build_model(self, dequeue_dict, batch_size, global_step):
        lens = dequeue_dict['lens']
        labels = dequeue_dict['labels']
        inputs = dequeue_dict['inputs']
        masks = dequeue_dict['masks']
        pts_lens = dequeue_dict['pts_lens']
        pts = dequeue_dict['pts']
        encoder_outputs, encoder_state_fw, encoder_state_bw = self.build_encoder(pts, pts_lens)
        per_h = self.build_middleware(encoder_state_fw, encoder_state_bw)
        decoder_outputs, decoder_state = self.build_decoder(encoder_outputs, inputs, per_h, lens, pts_lens)
        outputs = self.build_outputs(decoder_outputs, batch_size)
        loss = self.build_loss(outputs, labels, masks, batch_size)
        train_loss_summary = tf.summary.scalar(name="train_loss_summary", tensor=loss)
        valid_loss_summary = tf.summary.scalar(name="valid_loss_summary", tensor=loss)
        acc = self.build_acc(outputs, labels, masks)
        total_weights = tf.reduce_sum(masks)
        train_acc_summary = tf.summary.scalar(name="train_acc_summary", tensor=acc)
        valid_acc_summary = tf.summary.scalar(name="valid_acc_summary", tensor=acc)
        train_summary = tf.summary.merge([train_loss_summary, train_acc_summary])
        valid_summary = tf.summary.merge([valid_loss_summary, valid_acc_summary])
        saver = tf.train.Saver(tf.global_variables())
        train_op, optimizer_init_op = self.build_optimizer(loss, global_step)
        return loss, acc, total_weights, train_summary, valid_summary, train_op, saver, optimizer_init_op

    def build_encoder(self, pts, pts_lens):
        config = self.config

        flat_pts = tf.reshape(pts, [-1, 3])
        flat_inputs = tf.tanh(tf.add(tf.matmul(flat_pts, self.input_w), self.input_b))
        inputs = tf.reshape(flat_inputs, [config.batch_size, -1, config.hidden_dim])
        return tf.contrib.rnn.stack_bidirectional_dynamic_rnn(
            self.encoder_fw_cells,
            self.encoder_bw_cells,
            inputs,
            dtype=config.float_type,
            sequence_length=pts_lens)

    def build_middleware(self, encoder_state_fw, encoder_state_bw):
        cf = encoder_state_fw[0][0]
        cb = encoder_state_bw[0][0]
        hf = encoder_state_fw[0][1]
        hb = encoder_state_bw[0][1]
        h_input = tf.concat([hf, hb, tf.tanh(cf), tf.tanh(cb)], 1)
        per_h = tf.add(tf.matmul(h_input, self.m_per_w), self.m_per_b)
        return per_h

    def build_decoder(self, encoder_outputs, input_ids, per_h, lens, pts_lens):
        config = self.config

        Attention = {
            'Bahdanau': tf.contrib.seq2seq.BahdanauAttention,
            'Luong': tf.contrib.seq2seq.LuongAttention,
        }[config.attention_type]

        attention = Attention(
            config.attention_depth,
            encoder_outputs,
            memory_sequence_length=pts_lens,
            name='Attention')
        self.decoder_attn_cell = tf.contrib.seq2seq.DynamicAttentionWrapper(
            self.decoder_attn_cell,
            attention,
            config.attention_depth)
        self.decoder_cell = tf.contrib.rnn.MultiRNNCell([self.decoder_attn_cell, self.decoder_top_cell])

        embeded_inputs = tf.nn.embedding_lookup(self.embeddings, input_ids)
        per_h = tf.reshape(per_h, [config.batch_size, 1, config.encode_dim])
        h = tf.tile(per_h, [1, tf.shape(input_ids)[1], 1])
        inputs = tf.concat([embeded_inputs, h], axis=2)

        return tf.nn.dynamic_rnn(
            self.decoder_cell,
            inputs,
            dtype=config.float_type,
            sequence_length=lens)

    def build_outputs(self, hidden_outputs, batch_size):
        config = self.config

        flat_hidden_outputs = tf.reshape(hidden_outputs, [-1, config.hidden_dim])
        flat_output_logits = tf.add(tf.matmul(flat_hidden_outputs, self.output_w, transpose_b=True), self.output_b)
        return tf.reshape(flat_output_logits, [batch_size, -1, config.vocabulary_size])

    def build_loss(self, output_logits, labels, masks,  batch_size):
        return tf.reduce_sum(tf.contrib.seq2seq.sequence_loss(
            output_logits,
            labels,
            masks,
            average_across_timesteps=False,
            average_across_batch=False,
            name='sequence_loss')) / batch_size

    def build_acc(self, output_logits, labels, masks):
        predictions = tf.argmax(output_logits, axis=2)
        return tf.contrib.metrics.accuracy(predictions, labels, weights=masks)

    def build_optimizer(self, loss, global_step):
        config = self.config
        params = tf.trainable_variables()
        old_vars = set(tf.global_variables())
        optimizer = tf.train.AdamOptimizer(learning_rate=config.lr)
        grads, vars = zip(*optimizer.compute_gradients(loss, params))
        clipped_grads, norm = tf.clip_by_global_norm(grads, config.max_gradient_norm)
        train_op = optimizer.apply_gradients(zip(clipped_grads, params), global_step=global_step)
        optimizer_init_op = tf.variables_initializer(set(tf.global_variables()) - old_vars)
        return train_op, optimizer_init_op
